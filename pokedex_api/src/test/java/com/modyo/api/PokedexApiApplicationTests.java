	package com.modyo.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.modyo.api.dto.BasicDTO;
import com.modyo.api.dto.ListBasicDTO;
import com.modyo.api.dto.PageListPokemonDTO;
import com.modyo.api.dto.PokemonDTO;
import com.modyo.api.dto.settings.PageSizeDTO;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
@DisplayName("Test de pokedex api")
@SpringBootTest
class PokedexApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;	
	
	@Order(1)
	@DisplayName("Get all pokemon list ")
	@Test
	void getListPokemonTest() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		MvcResult result = mockMvc.perform(
				get("/pokedex/findAll").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		ListBasicDTO pokemonList = objectMapper.readerFor(ListBasicDTO.class)
				.readValue(result.getResponse().getContentAsString());

		int size = pokemonList.getCount();
		List<BasicDTO> list = pokemonList.getResults();
		
		log.info(String.format("size = %d", size));

		assertEquals(list.size(), 1118);

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}
	
		
	@Order(2)
	@DisplayName("Get paginated pokemon list size 3")
	@Test
	void getPaginatedListS3Test() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		log.debug("-- | test page size 3" );		
		
		PageSizeDTO request = new PageSizeDTO();
		request.setPageSize(3);
		
		mockMvc.perform(
				put("/pokedex/pageSize").contentType(Constants.APPLICATION_JSON).content(objectMapper.writeValueAsString(request))).andExpect(status().isOk());

		MvcResult result = mockMvc.perform(
				get("/pokedex/paginatedList").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		PageListPokemonDTO pokemonList = objectMapper.readerFor(PageListPokemonDTO.class)
				.readValue(result.getResponse().getContentAsString());

		int page = pokemonList.getPage();
		int size = pokemonList.getCount();
		List<PokemonDTO> list = pokemonList.getResults();
		
		log.info(String.format("page = %d", page));
		log.info(String.format("size = %d", size));

		assertEquals(list.size(), 3);

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}
	
	
	@Order(3)
	@DisplayName("Get paginated pokemon list size 5")
	@Test
	void getPaginatedListS5Test() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		log.debug("-- | test page size 5" );		
		
		PageSizeDTO request = new PageSizeDTO();
		request.setPageSize(5);
		
		mockMvc.perform(
				put("/pokedex/pageSize").contentType(Constants.APPLICATION_JSON).content(objectMapper.writeValueAsString(request))).andExpect(status().isOk());

		MvcResult result = mockMvc.perform(
				get("/pokedex/paginatedList").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		PageListPokemonDTO pokemonList = objectMapper.readerFor(PageListPokemonDTO.class)
				.readValue(result.getResponse().getContentAsString());

		int page = pokemonList.getPage();
		int size = pokemonList.getCount();
		List<PokemonDTO> list = pokemonList.getResults();
		
		log.info(String.format("page = %d", page));
		log.info(String.format("size = %d", size));

		assertEquals(list.size(), 5);

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}
	
	
	@Order(4)
	@DisplayName("Get paginated pokemon list page 3")
	@Test
	void getPaginatedListP3Test() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		log.debug("-- | test page size 5" );		
		
		PageSizeDTO request = new PageSizeDTO();
		request.setPageSize(5);
		
		mockMvc.perform(
				put("/pokedex/pageSize").contentType(Constants.APPLICATION_JSON).content(objectMapper.writeValueAsString(request))).andExpect(status().isOk());

		MvcResult result = mockMvc.perform(
				get("/pokedex/paginatedList/3").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		PageListPokemonDTO pokemonList = objectMapper.readerFor(PageListPokemonDTO.class)
				.readValue(result.getResponse().getContentAsString());

		int page = pokemonList.getPage();
		int size = pokemonList.getCount();
		
		log.info(String.format("page = %d", page));
		log.info(String.format("size = %d", size));

		assertEquals(page, 3);

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}	
		
	
	@Order(5)
	@DisplayName("Find pokemon # 1")
	@Test
	void findPokemonByIdTest() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		MvcResult result = mockMvc.perform(
				get("/pokedex/pokemonById/1").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		PokemonDTO pokemonDTO = objectMapper.readerFor(PokemonDTO.class)
				.readValue(result.getResponse().getContentAsString());

		log.info(String.format("id = %d", pokemonDTO.getId()));
		log.info(String.format("name = %s", pokemonDTO.getName()));

		assertEquals("bulbasaur", pokemonDTO.getName());

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}	
	
	@Order(6)
	@DisplayName("Find charizard")
	@Test
	void findPokemonByNameTest() throws Exception {
		
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		MvcResult result = mockMvc.perform(
				get("/pokedex/pokemonByName/charizard").contentType(Constants.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		String resultString = result.getResponse().getContentAsString();
		log.info(String.format("resultString = %s", resultString));

		PokemonDTO pokemonDTO = objectMapper.readerFor(PokemonDTO.class)
				.readValue(result.getResponse().getContentAsString());

		log.info(String.format("id = %d", pokemonDTO.getId()));
		log.info(String.format("name = %s", pokemonDTO.getName()));

		assertEquals("charizard", pokemonDTO.getName());

		log.debug(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));		
		
	}	

}
