package com.modyo.api.controller;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modyo.api.dto.EvolutionChainDTO;
import com.modyo.api.dto.ListBasicDTO;
import com.modyo.api.dto.PageListPokemonDTO;
import com.modyo.api.dto.PokemonDTO;
import com.modyo.api.dto.PokemonDetailDTO;
import com.modyo.api.dto.PokemonSpecieDTO;
import com.modyo.api.dto.settings.ImageTypeDTO;
import com.modyo.api.dto.settings.LanguageDTO;
import com.modyo.api.dto.settings.PageSizeDTO;
import com.modyo.api.exception.ServiceException;
import com.modyo.api.service.PokedexService;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@RestController
@RequestMapping(path = "/pokedex")
public class PokedexController {

	@Autowired
	PokedexService pokedexService;

	@GetMapping("/status")
	public ResponseEntity<String> status() {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String message = String.format("%1$s SERVICE ACTIVE!! - %2$tF %2$tr",
				Thread.currentThread().getStackTrace()[1].getClassName(), new Date());

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return ResponseEntity.ok(message);

	}

	@GetMapping("/findAll")
	public ResponseEntity<ListBasicDTO> findAll() {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ListBasicDTO list = pokedexService.getList();

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return ResponseEntity.ok(list);

	}

	@GetMapping(value = { "/paginatedList", "/paginatedList/", "/paginatedList/{page}" })
	public ResponseEntity<PageListPokemonDTO> paginatedList(@PathVariable Optional<Long> page) {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		int _page = page.orElse(1L).intValue();

		log.info(String.format("\t page = %d", _page));

		PageListPokemonDTO list = null;
		
		try {
			
			list = pokedexService.getPaginatedList(_page);
			
		} catch (ServiceException e) {
			log.error(e);
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return ResponseEntity.ok(list);

	}

	@GetMapping("/pokemonById/{id}")
	public ResponseEntity<Object> getPokemonById(@PathVariable Long id) throws Exception {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ResponseEntity<Object> response = null;
		
		PokemonDTO pokemon = null;
		
		try {			
			
			pokemon = pokedexService.getPokemon(id);
						
			if ( pokemon != null ) {
				response = ResponseEntity.ok(pokemon);
			} else {	
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body( String.format("Pokemon with id: %d, not found", id) );
			}
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}

	@GetMapping("/pokemonByName/{name}")
	public ResponseEntity<Object> getPokemonByName(@PathVariable String name) throws Exception {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ResponseEntity<Object> response = null;
		
		PokemonDTO pokemon = null;
		
		try {			
			
			pokemon = pokedexService.getPokemon(name);
			
			if ( pokemon != null ) {
				response = ResponseEntity.ok(pokemon);
			} else {	
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body( String.format("Pokemon with name: %s, not found", name) );
			}		
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}
	
	@GetMapping("/pokemonDetail/{id}")
	public ResponseEntity<Object> getPokemonDetail(@PathVariable Long id) throws ServiceException {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ResponseEntity<Object> response = null;
		
		PokemonDetailDTO pokemonDTO = null;
		
		try {			
			
			pokemonDTO = pokedexService.getPokemonDetail(id);
			
			if ( pokemonDTO != null ) {
				response = ResponseEntity.ok(pokemonDTO);
			} else {	
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body( String.format("Pokemon detail with id: %d, not found", id) );
			}		
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}	
	
	@GetMapping("/pokemonSpecie/{name}")
	public ResponseEntity<Object> getPokemonSpecie(@PathVariable String name) throws ServiceException {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ResponseEntity<Object> response = null;
		
		PokemonSpecieDTO pokemonSpecie = null;
		
		try {			
			
			pokemonSpecie = pokedexService.getPokemonSpecie(name);
			
			if ( pokemonSpecie != null ) {
				response = ResponseEntity.ok(pokemonSpecie);
			} else {	
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body( String.format("Pokemon specie with name: %s, not found", name) );
			}		
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}
	
	@GetMapping("/pokemonEvolution/{id}")
	public ResponseEntity<Object> getEvolutionChain(@PathVariable Long id) throws ServiceException {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		ResponseEntity<Object> response = null;
		
		EvolutionChainDTO evolutionChainDTO = null;
		
		try {			
			
			evolutionChainDTO = pokedexService.getEvolutionChain(id);
			
			if ( evolutionChainDTO != null ) {
				response = ResponseEntity.ok(evolutionChainDTO);
			} else {	
				response = ResponseEntity.status(HttpStatus.NOT_FOUND).body( String.format("Pokemon evolution with id: %d, not found", id) );
			}		
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}	
	
	@PutMapping("/language")
	public ResponseEntity<String> setLanguage(
			@RequestBody
			@Valid
			LanguageDTO request, 
			BindingResult result) {

		log.info("....");		
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));					
		
		if (result.hasErrors()) {

			StringBuilder msg = new StringBuilder();

			result.getAllErrors().forEach((error) -> {
				FieldError fieldError = ((FieldError) error);
				msg.append(String.format("%s : %s; ", fieldError.getField(), fieldError.getDefaultMessage()));
			});
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg.toString());
			
		} 
		
		ResponseEntity<String> response;

		try {			
			
			boolean changed = pokedexService.setLanguage(request.getLanguage());
			
			if (changed) {
				response = ResponseEntity.ok(String.format("LANGUAGE CHANGED : %s", request.getLanguage()));
			} else {
				response = ResponseEntity.ok(String.format("LANGUAGE NOT CHANGED"));
			}
			
		} catch (ServiceException e) {
			log.error(e);			
			response = ResponseEntity.badRequest().body(e.getMessage());
		}		

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}
	
	
	@PutMapping("/imageType")
	public ResponseEntity<String> setImageType(
			@RequestBody
			@Valid
			ImageTypeDTO request, 
			BindingResult result) {

		log.info("....");		
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));					
		
		if (result.hasErrors()) {

			StringBuilder msg = new StringBuilder();

			result.getAllErrors().forEach((error) -> {
				FieldError fieldError = ((FieldError) error);
				msg.append(String.format("%s : %s; ", fieldError.getField(), fieldError.getDefaultMessage()));
			});
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg.toString());
			
		} 

		boolean changed = pokedexService.setImageType(request.getImageType());

		ResponseEntity<String> response;

		if (changed) {
			response = ResponseEntity.ok(String.format("IMAGE TYPE CHANGED : %s", request.getImageType()));
		} else {
			response = ResponseEntity.ok(String.format("IMAGE TYPE NOT CHANGED"));
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}	
	
	
	
	@PutMapping("/pageSize")
	public ResponseEntity<String> setPageSize(
			@RequestBody
			@Valid
			PageSizeDTO request, 
			BindingResult result) {

		log.info("....");		
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));					
		
		if (result.hasErrors()) {

			StringBuilder msg = new StringBuilder();

			result.getAllErrors().forEach((error) -> {
				FieldError fieldError = ((FieldError) error);
				msg.append(String.format("%s : %s; ", fieldError.getField(), fieldError.getDefaultMessage()));
			});
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg.toString());
			
		} 
		
		log.info(String.format(" | --PageSize = %s", request.getPageSize()));
		
		boolean changed = pokedexService.setPageSize(request.getPageSize());

		ResponseEntity<String> response;

		if (changed) {
			response = ResponseEntity.ok(String.format("PAGESIZE CHANGED : %d", request.getPageSize()));
		} else {
			response = ResponseEntity.ok(String.format("PAGESIZE NOT CHANGED"));
		}

		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return response;

	}	
	
	
	@DeleteMapping("/cleanCache")
	public ResponseEntity<String> cleanCache(@RequestBody Optional<String> cacheName) {

		log.info("....");
		log.info(String.format(" | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		String _cacheName = cacheName.orElse("ALL");

		String message = pokedexService.cleanCache(_cacheName);
		
		log.info(String.format(" | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		return ResponseEntity.ok( message );

	}
	

}
