package com.modyo.api.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Controller
@RequestMapping(path = "/info")
public class InfoController {

	//gets html from a default 'resources/public' or 'resources/static' folder
	@GetMapping(value = "/index", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody	
	public String getWelcomePage(){
		  
		log.info(" entro a welcome");
		  
		return getTemplate();
	}
	
	private String getTemplate() {
		
		StringBuilder sb = new StringBuilder();
		
		sb
			.append("<html>")
			.append(" 	<header>")
			.append(" 	<title>Listado de servicios</title>")
			.append(" 	</header>")
			.append(" 	<body>")
			.append(" 		<br>")
			.append(" 		<h2>Listado de servicios</h2>")
			.append(" 			<pre> <h5><b>Nota :</b> Se entrego un archivo de postman para realizar el consumo de cada uno de los servicios</h5></pre>")									
			.append(" 		<ul>")
			.append(" 			<li><h4>Listar todos los pokemones</h4> ")
			.append(" 					<h5> - METODO GET : <a href=\"http://54.175.250.135:8080/pokedex/findAll\">http://54.175.250.135:8080/pokedex/findAll</a> </h5>")
			.append(" 			</li>")
			.append(" 			<li><h4>Listar paginada de pokemones</h4> ")
			.append(" 					<h5> - METODO GET : <a href=\"http://54.175.250.135:8080/pokedex/paginatedList\">http://54.175.250.135:8080/pokedex/paginatedList</a> </h5>")
			.append(" 			</li>")
			.append(" 			<li><h4>Encontrar pokemon por nombre</h4>")
			.append(" 					<h5> - METODO GET : <a href=\"http://54.175.250.135:8080/pokedex/pokemonByName/bulbasaur\">http://54.175.250.135:8080/pokedex/pokemonByName/bulbasaur</a> </h5>")
			.append(" 			</li>")
			.append(" 			<li><h4>Encontrar pokemon por id</h4>")
			.append(" 					<h5> - METODO GET : <a href=\"http://54.175.250.135:8080/pokedex/pokemonById/2\">http://54.175.250.135:8080/pokedex/pokemonByName/2</a> </h5>")
			.append(" 			</li>")
			.append(" 			<li><h4>Encontrar detalle del pokemon por id></h4>")
			.append(" 					<h5> - METODO GET : <a href=\"http://54.175.250.135:8080/pokedex/pokemonDetail/1\">http://54.175.250.135:8080/pokedex/pokemonDetail/1</a>")
			.append(" 			</li>")
			.append(" 			<li><h4>Configurar el tama�o de pagina</h4>")
			.append(" 				<h5>  -  METODO PUT <a href=\"#\">http://54.175.250.135:8080/pokedex/pageSize</a>")
			.append(" 			</li>")
			.append(" 			<li><h4>Configurar el idioma de la descripci�n</h4>")
			.append(" 				<h5>  -  METODO PUT <a href=\"#\">http://54.175.250.135:8080/pokedex/language</a>")
			.append(" 			</li>")
			.append(" 			<li><h4>Configurar el tipo de imagen</h4>")
			.append(" 				<h5>  -  METODO PUT <a href=\"#\">http://54.175.250.135:8080/pokedex/imageType</a>")
			.append(" 			</li>")			
			.append(" 			<li><h4>Limpiar el cache</h4>")
			.append(" 				<h5>  -  METODO DELETE <a href=\"#\">http://54.175.250.135:8080/pokedex/cleanCache</a>")
			.append(" 			</li>")			
			.append(" 		</ul>")
			.append(" 		<div>")			
			.append(" 			<pre>")			
			.append(" 	<h5>-Javier Andres Gomez-</h5>")			
			.append(" 			</pre>")									
			.append(" 		</div>")			
			.append(" 	</body>")
			.append("</html>");
		
		return sb.toString();
			
	}
	
}
