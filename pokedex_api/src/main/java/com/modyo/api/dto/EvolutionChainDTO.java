package com.modyo.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EvolutionChainDTO {
	
	private long id;

	private ChainLinkDTO chain;
	
}
