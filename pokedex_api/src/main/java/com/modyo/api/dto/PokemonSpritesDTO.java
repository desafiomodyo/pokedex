package com.modyo.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonSpritesDTO {

	private String front_default;

	private String front_shiny;

	private String front_female;

	private String front_shiny_female;

	private String back_default;

	private String back_shiny;

	private String back_female;

	private String back_shiny_female;

}
