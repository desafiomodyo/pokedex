package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ListBasicDTO {

	private int count;

	private List<BasicDTO> results;
	
}
