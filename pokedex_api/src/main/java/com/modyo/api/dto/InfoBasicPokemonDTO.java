package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InfoBasicPokemonDTO extends BasicDTO {

		private long height;

		private long weight;		

		private long order;
		
		private long base_experience;

		private boolean is_default;

		private List<PokemonAbilityDTO> abilities;
		
		private PokemonSpritesDTO sprites;
		
		private List<PokemonTypeDTO> types;
			
}
