package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonDTO extends BasicDTO {

	private long height;

	private long weight;		

	private long order;
	
	private long base_experience;

	private String image;
	
	private List<PokemonTypeDTO> types;
	
	private List<PokemonAbilityDTO> abilities;	
	
}
