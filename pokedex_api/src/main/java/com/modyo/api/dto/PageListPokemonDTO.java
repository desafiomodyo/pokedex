package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PageListPokemonDTO {

	private int count;
	
	private int page;

	private List<PokemonDTO> results;
	
}
