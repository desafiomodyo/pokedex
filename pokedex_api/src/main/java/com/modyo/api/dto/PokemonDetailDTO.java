package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonDetailDTO extends PokemonDTO {

	String description;
	
	//String evolution;
	
	List<NameDTO> evolutions;
	
}
