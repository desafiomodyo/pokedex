package com.modyo.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonTypeDTO {

	private NameDTO type;
	
}
