package com.modyo.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonAbilityDTO {
	
	private NameDTO ability;
	
	private boolean is_hidden;

}
