package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChainLinkDTO {

	private boolean is_baby;

	private NameDTO species;

	private List<ChainLinkDTO> evolves_to;	
	
}
