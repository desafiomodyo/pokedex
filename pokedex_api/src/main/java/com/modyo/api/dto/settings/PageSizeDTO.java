package com.modyo.api.dto.settings;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PageSizeDTO {

	@Min(value = 1, message = "Page Size should not be less than 1")
    @Max(value = 20, message = "Page Size should not be greater than 20")
	@NotNull(message = "Page size is mandatory")
	private int pageSize;
	
}
