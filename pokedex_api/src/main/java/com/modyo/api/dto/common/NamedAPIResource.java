package com.modyo.api.dto.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NamedAPIResource {

	private String name;
		
	private String url;
	
}
