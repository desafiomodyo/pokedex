package com.modyo.api.dto.common;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NamedAPIResourceList {

	private List<NamedAPIResource> results;
	
}
