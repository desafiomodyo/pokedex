package com.modyo.api.dto.settings;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LanguageDTO {

	@NotBlank(message = "Language is mandatory")
	private String language;
	
}
