package com.modyo.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FlavorDTO {

	private String flavor_text;

	private NameDTO language;

	private NameDTO version;
	
}
