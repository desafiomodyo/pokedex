package com.modyo.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PokemonSpecieDTO extends NameDTO {
	
	private int base_happiness;
	
	private int capture_rate;

	private UrlDTO evolution_chain;
	
	private NameDTO color;
	
	private NameDTO evolves_from_species;
	
    private List<FlavorDTO> flavor_text_entries;
                            
}
