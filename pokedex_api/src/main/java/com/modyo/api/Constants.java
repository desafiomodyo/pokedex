package com.modyo.api;

public class Constants {
	
	public static final String APPLICATION_JSON = "application/json";
	
	// Constantes para el manejo de cache
	public static final String CACHE_KEY_POKEMON_LIST = "poke-list";
	
	public static final String CACHE_KEY_POKEMON_NAME = "poke-name";
	
	public static final String CACHE_KEY_POKEMON_ID = "poke-id";
	
	public static final String CACHE_KEY_POKEMON_SPECIE = "poke-specie";
	
	public static final String CACHE_KEY_POKEMON_IMAGE = "poke-image";
	
	public static final String CACHE_KEY_POKEMON_EVOLUTION_CHAIN = "poke-evolution-chain";
		
	public static final String CACHE_KEY_POKEMON_LANGUAGE = "poke-language";
	
	public static final String CACHE_KEY_PAGINATED_LIST = "poke-paginated-list";
	
	public static final String CACHE_KEY_DETAIL = "poke-detail";
			
	
	// Constantes para el manejo de imagenes
	public static final String IMAGE_BACK_SHINY_FEMALE = "back_shiny_female";

	public static final String IMAGE_BACK_SHINY = "back_shiny";

	public static final String IMAGE_BACK_FEMALE = "back_female";

	public static final String IMAGE_BACK_DEFAULT = "back_default";

	public static final String IMAGE_FRONT_SHINY_FEMALE = "front_shiny_female";

	public static final String IMAGE_FRONT_SHINY = "front_shiny";

	public static final String IMAGE_FRONT_FEMALE = "front_female";

	public static final String IMAGE_FRONT_DEFAULT = "front_default";

	public static final String[] IMAGE_TYPE_LIST = new String[] { IMAGE_FRONT_DEFAULT, IMAGE_FRONT_FEMALE,
			IMAGE_FRONT_SHINY, IMAGE_FRONT_SHINY_FEMALE, IMAGE_FRONT_SHINY_FEMALE, IMAGE_BACK_DEFAULT, IMAGE_BACK_FEMALE, IMAGE_BACK_SHINY, IMAGE_BACK_SHINY_FEMALE};
	
}
