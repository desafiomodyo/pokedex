package com.modyo.api.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.modyo.api.Constants;
import com.modyo.api.dto.BasicDTO;
import com.modyo.api.dto.ChainLinkDTO;
import com.modyo.api.dto.EvolutionChainDTO;
import com.modyo.api.dto.FlavorDTO;
import com.modyo.api.dto.InfoBasicPokemonDTO;
import com.modyo.api.dto.ListBasicDTO;
import com.modyo.api.dto.NameDTO;
import com.modyo.api.dto.PageListPokemonDTO;
import com.modyo.api.dto.PokemonDTO;
import com.modyo.api.dto.PokemonDetailDTO;
import com.modyo.api.dto.PokemonSpecieDTO;
import com.modyo.api.dto.settings.LanguageDTO;
import com.modyo.api.exception.ServiceException;
import com.modyo.api.external.client.PokeApiClient;
import com.modyo.api.service.PokedexService;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Service
public class PokedexServiceImpl implements PokedexService {

	@Autowired
	private PokeApiClient pokeApiClient;

	@Autowired
	private CacheManager cacheManager;
	
	@Value("${pokeapi.imageType:front_default}")
	private String imageType;

	@Value("${pokeapi.language:en}")
	private String language;

	@Value("${pokeapi.pageSize:20}")
	private int limit;

	private static int MAX = 1180;

	public boolean setImageType(String imageType) {
				
		String imageTypeList = Arrays.toString(Constants.IMAGE_TYPE_LIST);
		
		boolean find = imageTypeList.contains( imageType );
		
		if ( find ) {
			
			this.imageType = imageType;
			
		}
		
		return find;
		
	}

	public boolean setLanguage(String language) throws ServiceException {

		boolean find = false;

		try {

			LanguageDTO languageDTO = pokeApiClient.getLanguage(language);
			
			find = languageDTO != null;
			
			if ( find ) {
				
				this.language = language;
				
			}			
			
		} catch (RestClientException e) {
			throw new ServiceException(e);
		}
		
		return find;
		
	}	

	public boolean setPageSize(int pageSize) {

		if (pageSize > 0) {
			this.limit = pageSize;
			
			cleanCache(Constants.CACHE_KEY_PAGINATED_LIST);

			return true;
		}

		return false;
	}

	public String cleanCache(String cacheName) {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		if (log.isDebugEnabled())
			log.debug(String.format("\t\t cacheName = %s", cacheName));

		String message;

		if ("ALL".equals(cacheName)) {

			cacheManager.getCacheNames().forEach(name -> cleanCache(name));
			message = "ALL CACHE CLEANED!!!";

		} else {

			if (cacheManager.getCache(cacheName) != null) {

				cacheManager.getCache(cacheName).clear();
				message = String.format("CACHE %s CLEANED!!!", cacheName);

			} else {

				message = String.format("CACHE %s NOT CLEANED!!!", cacheName);

			}

		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return message;

	}

	public void cleanAllCache() {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		cacheManager.getCacheNames().forEach(name -> cleanCache(name));

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

	}

	public ListBasicDTO getList() {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		List<BasicDTO> results = pokeApiClient.getList(0, MAX);

		ListBasicDTO result = new ListBasicDTO();
		result.setResults(results);
		result.setCount(results.size());

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;

	}
	
	@Cacheable(value = Constants.CACHE_KEY_PAGINATED_LIST)
	public PageListPokemonDTO getPaginatedList(int page) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		int offset = getOffset(page);

		if (log.isDebugEnabled())
			log.debug(String.format("\t\t offset = %d ", offset));

		List<BasicDTO> list = pokeApiClient.getList(offset, limit);

		List<PokemonDTO> results = null;

			results = list.stream().map(p -> {
				
				try {
					return getPokemon(p.getId());
				} catch (ServiceException e) {
					log.error(e);
				}
				
				return null;
				
			}).collect(Collectors.toList());

		PageListPokemonDTO result = new PageListPokemonDTO();
		result.setResults(results);
		result.setCount(results.size());
		result.setPage(page);

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;

	}

	public PokemonDTO getPokemon(Long id) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		InfoBasicPokemonDTO infoBasic = null;
		PokemonDTO pokemon = null;
		
		try {

			infoBasic = getInfoBasicById(id);
			
			pokemon = new PokemonDTO();
			BeanUtils.copyProperties(infoBasic, pokemon);
			
			String image = extractImage( infoBasic, imageType );
			pokemon.setImage(image);
			
			
		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return pokemon;

	}

	public PokemonDTO getPokemon(String name) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		InfoBasicPokemonDTO infoBasic = null;
		PokemonDTO pokemon = null;
		
		try {

			infoBasic = getInfoBasicByName(name);
			
			pokemon = new PokemonDTO();
			BeanUtils.copyProperties(infoBasic, pokemon);
			
			String image = extractImage( infoBasic, imageType );
			pokemon.setImage(image);
			
		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return pokemon;

	}
	
	@Cacheable(value = Constants.CACHE_KEY_DETAIL)
	public PokemonDetailDTO getPokemonDetail(Long id) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		InfoBasicPokemonDTO infoBasic = null;
		PokemonDetailDTO pokemonDetail = null;
		
		try {

			infoBasic = getInfoBasicById(id);
			
			pokemonDetail = new PokemonDetailDTO();
			BeanUtils.copyProperties(infoBasic, pokemonDetail);
			
			String image = extractImage( infoBasic, imageType );
			pokemonDetail.setImage(image);

			PokemonSpecieDTO infoSpecie = getPokemonSpecie(pokemonDetail.getName());
			
			String description = extractDescription( infoSpecie, language );
			pokemonDetail.setDescription(description);
			
			List<NameDTO> evolutions = extractEvolutions( infoSpecie );			
			pokemonDetail.setEvolutions(evolutions);			

		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return pokemonDetail;

	}	
	
	public PokemonSpecieDTO getPokemonSpecie(String name) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		PokemonSpecieDTO result = null;

		try {

			result = pokeApiClient.getInfoSpecie(name);

		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;

	}
		
	public EvolutionChainDTO getEvolutionChain(Long id) throws ServiceException {
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		EvolutionChainDTO result = null;

		try {

			result = pokeApiClient.getEvolutionChain(id);

		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;		
		
	}
	
	public LanguageDTO getLanguage(String language) throws ServiceException {
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		LanguageDTO result = null;

		try {

			result = pokeApiClient.getLanguage(language);

		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;		
		
	}
	
	private InfoBasicPokemonDTO getInfoBasicById(Long id) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		InfoBasicPokemonDTO infoBasic = null;
		
		try {

			infoBasic = pokeApiClient.getInfoBasic(id);
						
		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return infoBasic;

	}	
	
	private InfoBasicPokemonDTO getInfoBasicByName(String name) throws ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		InfoBasicPokemonDTO infoBasic = null;
		
		try {

			infoBasic = pokeApiClient.getInfoBasic(name);
						
		} catch (RestClientException e) {
			throw new ServiceException(e);
		}

		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return infoBasic;

	}	
	
	private String extractImage( InfoBasicPokemonDTO infoBasic, String imageType ) {
		
		String image = null;
		
		if ( Constants.IMAGE_BACK_SHINY_FEMALE.equals( imageType ) ) {
			image = infoBasic.getSprites().getBack_shiny_female();
		} else if ( Constants.IMAGE_BACK_SHINY.equals( imageType ) ) {
			image = infoBasic.getSprites().getBack_shiny();
		} else if ( Constants.IMAGE_BACK_FEMALE.equals( imageType ) ) {
			image = infoBasic.getSprites().getBack_female();
		} else if ( Constants.IMAGE_BACK_DEFAULT.equals( imageType ) ) {
			image = infoBasic.getSprites().getBack_default();
		} else if ( Constants.IMAGE_FRONT_SHINY_FEMALE.equals( imageType ) ) {
			image = infoBasic.getSprites().getFront_shiny_female();
		} else if ( Constants.IMAGE_FRONT_SHINY.equals( imageType ) ) {
			image = infoBasic.getSprites().getFront_shiny();
		} else if ( Constants.IMAGE_FRONT_FEMALE.equals( imageType ) ) {
			image = infoBasic.getSprites().getFront_female();
		}
		
		if ( image == null ) {
			image = infoBasic.getSprites().getFront_default();
		}
		
		return image;
		
	}
	
	private String extractDescription( PokemonSpecieDTO infoSpecie, String language) {
		
		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		String description = null;
		
		Optional<FlavorDTO> flavor = infoSpecie.getFlavor_text_entries().stream()
				.filter( c -> language.equals(c.getLanguage().getName()) ).findFirst();
		
		if ( flavor.isPresent() ) {
			description = flavor.get().getFlavor_text();			
		} else {
			description = String.format("Description not found for language %s", language);
		}
		
		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		return description;		
		
	}	
	
	private List<NameDTO> extractEvolutions( PokemonSpecieDTO infoSpecie ) throws NumberFormatException, ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String evolutionChainUrl = infoSpecie.getEvolution_chain().getUrl();
		
		String[] evolutionChainArray  = evolutionChainUrl.split("/");
		
		String idEvolutionChain = evolutionChainArray[ evolutionChainArray.length - 1 ];
		log.info(String.format("-- | idEvolutionChain = %s", idEvolutionChain) );
		
		EvolutionChainDTO evolutionChain = getEvolutionChain( Long.valueOf( idEvolutionChain ) );
		
		ChainLinkDTO chain = evolutionChain.getChain();
		
		List<NameDTO> evolutions = findEvolutions( infoSpecie.getName(), chain );		
		
		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		return evolutions;
		
	}
	
	private List<NameDTO> findEvolutions( String pokemonName, ChainLinkDTO chain ) {

		List<NameDTO> evolutions = new ArrayList<NameDTO>(0);
		
		if ( chain != null ) {
			
			Optional<ChainLinkDTO> next = chain.getEvolves_to().stream().findFirst();

			if ( next.isPresent() ) {					
			
				NameDTO evolution = next.get().getSpecies();
				
				if ( pokemonName.equals( chain.getSpecies().getName() ) ) {
					
					evolutions.add( evolution );
					
				} 	
				
				List<NameDTO> nextEvolutions = findEvolutions( evolution.getName(), next.get() );
				
				if ( ! nextEvolutions.isEmpty() ) {
					evolutions.addAll( nextEvolutions );
				}
				
			}					
			
		}
				
		return evolutions;
				
	}
	
	
	private int getOffset(int page) {
	
		int offset = 0;
	
		if (page > 0) {
	
			offset = (page - 1) * limit;
	
			if (offset > MAX) {
				offset = MAX;
			}
		}
	
		return offset;
	}
	

	
	/*
	private String extractEvolution( PokemonSpecieDTO infoSpecie ) throws NumberFormatException, ServiceException {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String evolutionChainUrl = infoSpecie.getEvolution_chain().getUrl();
		
		String[] evolutionChainArray  = evolutionChainUrl.split("/");
		
		String idEvolutionChain = evolutionChainArray[ evolutionChainArray.length - 1 ];
		log.info(String.format("-- | idEvolutionChain = %s", idEvolutionChain) );
		
		EvolutionChainDTO evolutionChain = getEvolutionChain( Long.valueOf( idEvolutionChain ) );
		
		ChainLinkDTO chain = evolutionChain.getChain();
		
		String evolution = findEvolution( infoSpecie.getName(), chain );
		
		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		return evolution;
		
	}	
	
	private String findEvolution( String pokemonName, ChainLinkDTO chain ) {

		log.info(String.format("-- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		log.info(String.format("-- | pokemonName = %s", pokemonName) );		

		String evolutionName = null;
		boolean find = false;
		
		if ( chain != null ) {
			
			String specieName = chain.getSpecies().getName();
			
			Optional<ChainLinkDTO> next = chain.getEvolves_to().stream().findFirst();

			if ( next.isPresent() ) {					
			
				if ( pokemonName.equals( specieName ) ) {
					
					evolutionName = next.get().getSpecies().getName();
					find = true;
				} 	
				
				String nextEvolution = findEvolution( next.get().getSpecies().getName(), next.get() );
				
				if ( nextEvolution != null ) {
					
					if (find) {
						evolutionName += " --> " + nextEvolution;
					} else {
						evolutionName = nextEvolution;
					}
					
				}
				
			}					
			
		}
				
		log.info(String.format("-- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		return evolutionName;
		
	}
	*/	

}
