package com.modyo.api.service;

import com.modyo.api.dto.EvolutionChainDTO;
import com.modyo.api.dto.ListBasicDTO;
import com.modyo.api.dto.PageListPokemonDTO;
import com.modyo.api.dto.PokemonDTO;
import com.modyo.api.dto.PokemonDetailDTO;
import com.modyo.api.dto.PokemonSpecieDTO;
import com.modyo.api.exception.ServiceException;

public interface PokedexService {

	public ListBasicDTO getList();

	public PageListPokemonDTO getPaginatedList(int page) throws ServiceException;

	public PokemonDTO getPokemon(Long id) throws ServiceException;

	public PokemonDTO getPokemon(String name) throws ServiceException;

	public PokemonDetailDTO getPokemonDetail(Long id) throws ServiceException;	
	
	public PokemonSpecieDTO getPokemonSpecie(String name) throws ServiceException;
	
	public EvolutionChainDTO getEvolutionChain(Long id) throws ServiceException;
	
	public boolean setLanguage(String language) throws ServiceException;
	
	public boolean setImageType(String imageType);
		
	public boolean setPageSize( int pageSize );
	
	public String cleanCache( String cacheName );

}
