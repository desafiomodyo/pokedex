package com.modyo.api.external.client;

import java.util.List;

import com.modyo.api.dto.BasicDTO;
import com.modyo.api.dto.EvolutionChainDTO;
import com.modyo.api.dto.InfoBasicPokemonDTO;
import com.modyo.api.dto.PokemonSpecieDTO;
import com.modyo.api.dto.settings.LanguageDTO;

public interface PokeApiClient {
	
	public List<BasicDTO> getList(int offset, int limit);	
	
	public InfoBasicPokemonDTO getInfoBasic(Long id);
	
	public InfoBasicPokemonDTO getInfoBasic(String name);
	
	public PokemonSpecieDTO getInfoSpecie(String name);	
	
	public EvolutionChainDTO getEvolutionChain(Long id);
	
	public LanguageDTO getLanguage(String language);

}

