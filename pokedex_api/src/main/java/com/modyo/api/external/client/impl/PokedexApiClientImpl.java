package com.modyo.api.external.client.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.modyo.api.Constants;
import com.modyo.api.dto.BasicDTO;
import com.modyo.api.dto.EvolutionChainDTO;
import com.modyo.api.dto.InfoBasicPokemonDTO;
import com.modyo.api.dto.PokemonSpecieDTO;
import com.modyo.api.dto.common.NamedAPIResource;
import com.modyo.api.dto.common.NamedAPIResourceList;
import com.modyo.api.dto.settings.LanguageDTO;
import com.modyo.api.external.client.PokeApiClient;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Service
public class PokedexApiClientImpl implements PokeApiClient {

	@Autowired
	private RestTemplate restClient;

	@Value("${pokeapi.url}")
	private String urlBase;

	/*
	 * Metodo para obtener la lista completa de pokemones
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_LIST)
	public List<BasicDTO> getList(int offset, int limit) {

		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String endpoint = String.format("%s/pokemon/?offset=%d&limit=%d", urlBase, offset, limit);

		if (log.isDebugEnabled())
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		
		NamedAPIResourceList list = restClient.getForObject(endpoint, NamedAPIResourceList.class);

		List<BasicDTO> results = list.getResults().stream().map(p -> {
			
			BasicDTO basicDTO = new BasicDTO();

			basicDTO.setName(p.getName());
			basicDTO.setId(getId(p));
			
			return basicDTO;
			
		}).collect(Collectors.toList());

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return results;

	}

	/*
	 * Metodo para obtener la informacion basica de un pokemon por id
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_ID)
	public InfoBasicPokemonDTO getInfoBasic(Long id) {

		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String endpoint = String.format("%s/pokemon/%s", urlBase, id);

		if (log.isDebugEnabled()) {
			log.info(String.format("-- --\t id = %d ", id));
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		}

		InfoBasicPokemonDTO detail = null;
		
		try {
			
			detail = restClient.getForObject(endpoint, InfoBasicPokemonDTO.class);
			
		} catch (NotFound e) {
			
			log.info( String.format("Pokemon with id %d not found", id) );
			
		} catch (RestClientException e) {
			throw e;
		}

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return detail;

	}

	/*
	 * Metodo para obtener la informacion basica de un pokemon por nombre
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_NAME)
	public InfoBasicPokemonDTO getInfoBasic(String name) {

		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String endpoint = String.format("%s/pokemon/%s", urlBase, name);

		if (log.isDebugEnabled()) {
			log.info(String.format("-- --\t name = %s", name));
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		}

		InfoBasicPokemonDTO detail = restClient.getForObject(endpoint, InfoBasicPokemonDTO.class);

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return detail;

	}
	
	/*
	 * Metodo para obtener el id de un pokemon a partir de la url
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_SPECIE)	
	public PokemonSpecieDTO getInfoSpecie(String name) {

		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String endpoint = String.format("%s/pokemon-species/%s", urlBase, name);

		if (log.isDebugEnabled()) {
			log.info(String.format("-- --\t name = %s", name));
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		}

		PokemonSpecieDTO result = restClient.getForObject(endpoint, PokemonSpecieDTO.class);

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;


	}
	
	
	/*
	 * Metodo para obtener la cedna de evoluciones de un pokemon
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_EVOLUTION_CHAIN)
	public EvolutionChainDTO getEvolutionChain(Long id) {
		
		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		String endpoint = String.format("%s/evolution-chain/%d", urlBase, id);

		if (log.isDebugEnabled()) {
			log.info(String.format("-- --\t id = %d ", id));
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		}

		EvolutionChainDTO result = restClient.getForObject(endpoint, EvolutionChainDTO.class);

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;		
		
	}
	
	
	/*
	 * Metodo para obtener la cedna de evoluciones de un pokemon
	 */
	@Cacheable(value = Constants.CACHE_KEY_POKEMON_LANGUAGE)
	public LanguageDTO getLanguage(String language) {
		
		log.info(String.format("-- -- | INIT METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		String endpoint = String.format("%s/language/%s", urlBase, language);

		if (log.isDebugEnabled()) {
			log.info(String.format("-- --\t language = %s", language));
			log.debug(String.format("-- --\t endpoint = %s", endpoint));
		}
		
		LanguageDTO result = null;		
		
		try {
			
			result = restClient.getForObject(endpoint, LanguageDTO.class);
			
		} catch (NotFound e) {
			
			log.info( String.format("Language %snot found", language) );
			
		} catch (RestClientException e) {
			throw e;
		}

		log.info(String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return result;		
		
	}
	


	/*
	 * Metodo para obtener el id de un pokemon a partir de la url
	 */
	private long getId(NamedAPIResource namedAPIResource) {

		if (log.isDebugEnabled())
			log.info(String.format("-- -- | INIT METHOD %s",
					Thread.currentThread().getStackTrace()[1].getMethodName()));

		long id = 0;

		try {

			String[] split = namedAPIResource.getUrl().split("/");
			String val = split[split.length - 1];

			id = Long.parseLong(val);

		} catch (NumberFormatException e) {
			log.error("Could not extract the id from the pokemon url");
		}

		if (log.isDebugEnabled())
			log.info(
					String.format("-- -- | END METHOD %s", Thread.currentThread().getStackTrace()[1].getMethodName()));

		return id;

	}	

}
