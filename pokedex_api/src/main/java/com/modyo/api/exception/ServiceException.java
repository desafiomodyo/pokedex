package com.modyo.api.exception;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3634467315444975943L;

	public ServiceException( ) {
		
	}
	
	public ServiceException( Exception e ) {		
		super(e);		
	}
	
}
