package com.modyo.api;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@EnableCaching
@Configuration
public class AppConfig {

	
//	@Bean
//	public RestTemplate restTemplate() {
//				
//		return new RestTemplate();
//	}
	
	@Bean
	public RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
		
	    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
	    
	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
	    
	    Registry<ConnectionSocketFactory> socketFactoryRegistry = 
	    	      RegistryBuilder.<ConnectionSocketFactory> create()
	    	      .register("https", sslsf)
	    	      .register("http", new PlainConnectionSocketFactory())
	    	      .build();
	    
	    BasicHttpClientConnectionManager connectionManager = 
	    	      new BasicHttpClientConnectionManager(socketFactoryRegistry);
	    
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
	    	      .setConnectionManager(connectionManager).build();
	    
	    HttpComponentsClientHttpRequestFactory requestFactory = 
	    	      new HttpComponentsClientHttpRequestFactory(httpClient);
		
		return new RestTemplate(requestFactory);
	}
	
	
	@Bean
    public CacheManager cacheManager() {
				
        return new ConcurrentMapCacheManager(
        		Constants.CACHE_KEY_POKEMON_LIST,
        		Constants.CACHE_KEY_POKEMON_ID,
        		Constants.CACHE_KEY_POKEMON_NAME,
        		Constants.CACHE_KEY_POKEMON_SPECIE,
        		Constants.CACHE_KEY_POKEMON_IMAGE,
        		Constants.CACHE_KEY_POKEMON_EVOLUTION_CHAIN,
        		Constants.CACHE_KEY_POKEMON_LANGUAGE,        		        	
        		Constants.CACHE_KEY_PAGINATED_LIST,
        		Constants.CACHE_KEY_DETAIL
        		);
    }

}


